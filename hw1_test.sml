use "hw1.sml";


(* test for is_older *)
val test1 = is_older ((1,2,3),(2,3,4)) = true
val test2 = is_older ((1,1,1),(1,1,1)) = false;
val test3 = is_older ((1,1,1),(1,1,2)) = true;


(* test for number_in_month *)
val test0 = number_in_month ([(2012,2,28),(2013,12,1)],2) = 1
val test1 = number_in_month([], 2) = 0
val test2 = number_in_month([(2012, 2, 28), (2013, 12, 1)], 2) = 1
val test3 = number_in_month([(2012, 2, 28), (2013, 12, 1), (2013, 2, 1)], 2) = 2
val test4 = number_in_month([(2012, 1, 28), (2013, 12, 1)], 2) = 0
val test5 = number_in_month([(2013, 2, 28), (2013, 2, 1), (2013, 2, 14)], 2) = 3
val test6 = number_in_month([(2012, 2, 29), (2012, 2, 28), (2013, 2, 1)], 3) = 0
val test7 = number_in_month ([(2012,2,28),(2013,12,1),(2013,2,1)],2) = 2
val test8 = number_in_month ([(2012,1,28),(2013,12,1)],2) = 0


(* test for number_number_months *)
val test0 = number_in_months ([(2012,2,28),(2013,12,1),(2011,3,31),(2011,4,28)],[2,3,4]) = 3
val test1 = number_in_months([], [2, 3, 4]) = 0
val test2 = number_in_months([(2012, 2, 28), (2013, 12, 1)], [2, 3, 4]) = 1
val test3 = number_in_months([(2012, 2, 28), (2013, 12, 1), (2011, 3, 31), (2011, 4, 28)], [2, 3, 4]) = 3
val test4 = number_in_months([(2012, 1, 28), (2013, 12, 1)], [2, 3, 4]) = 0
val test5 = number_in_months([(2013, 2, 28), (2013, 2, 1), (2013, 2, 14)], [2, 3, 4]) = 3
val test6 = number_in_months([(2012, 2, 28), (2012, 3, 1), (2013, 4, 1)], [2, 3, 4]) = 3
val test7 = number_in_months([(2012, 2, 28), (2012, 3, 1), (2013, 4, 1)], [1, 5, 6]) = 0

(* test for dates_in_month *)
val test0 = dates_in_month ([(2012,2,28),(2013,12,1)],2) = [(2012,2,28)]
val test1 = dates_in_month ([(2023,3,8),(2023,3,9),(2023,4,1)], 3) = [(2023,3,8), (2023,3,9)]
val test2 = dates_in_month ([(2023,3,8),(2023,3,9),(2023,4,1)], 4) = [(2023,4,1)]
val test3 = dates_in_month ([], 2) = []
val test4 = dates_in_month ([(2012,2,28),(2013,12,1)],2) = [(2012,2,28)]
val test5 = dates_in_month ([(2023,3,8),(2023,3,9),(2023,4,1)], 2) = []

(* test for dates_in_months *)
val test0 = dates_in_months ([(2012,2,28),(2013,12,1),(2011,3,31),(2011,4,28)],[2,3,4]) = [(2012,2,28),(2011,3,31),(2011,4,28)]
val test1 = dates_in_months ([(2000,1,1),(2001,2,2),(2002,3,3)], [1,2,3]) = [(2000,1,1),(2001,2,2),(2002,3,3)];
val test2 = dates_in_months ([(1999,11,30),(2000,12,31),(2001,1,1)], [1,2,3,12]) = [(2000,12,31),(2001,1,1)];
val test3 = dates_in_months ([], [1,2,3]) = [];
val test4 = dates_in_months ([(2022,6,15),(2022,7,20)], []) = [];
val test5 = dates_in_months ([(1998,8,31),(2000,2,29),(2022,11,30)], [6,7,8]) = [(1998,8,31),(2022,11,30)];

(* test for get_nth *)
val test0 = get_nth (["hi", "there", "how", "are", "you"], 2) = "there"
val test1 = get_nth (["apple", "banana", "cherry"], 1) = "apple"
val test2 = get_nth ([], 5) = ""
val test3 = get_nth (["one"], 1) = "one"
val test4 = get_nth (["a", "b", "c", "d"], 3) = "c"
val test5 = get_nth (["alpha", "beta", "gamma", "delta"], 4) = "delta"

(* test for the date_to_string *)
val test0 = date_to_string (2013, 6, 1) = "June 1, 2013"
val test1 = date_to_string(2023, 3, 8) = "March 8, 2023";
val test2 = date_to_string(2022, 12, 31) = "December 31, 2022";
val test3 = date_to_string(2022, 1, 1) = "January 1, 2022";
val test4 = date_to_string(2023, 2, 14) = "February 14, 2023";
val test5 = date_to_string(2021, 11, 11) = "November 11, 2021";
val test6 = date_to_string(2000, 5, 31) = "May 31, 2000";

(* test for the number_before_reaching_sum *)
val test0 = number_before_reaching_sum (10, [1,2,3,4,5]) = 3
val test1 = number_before_reaching_sum(5, [1, 1, 1, 1, 1, 1]) = 4;
val test2 = number_before_reaching_sum(20, [2, 4, 6, 8, 10, 12]) = 3;
val test3 = number_before_reaching_sum(100, [10, 20, 30, 40, 50, 60]) = 3;
val test4 = number_before_reaching_sum(15, [1, 2, 3, 4, 5, 6, 7]) = 4;
val test5 = number_before_reaching_sum(50, [5, 10, 15, 20, 25, 30, 35]) = 3;
val test6 = number_before_reaching_sum(12, [3, 3, 3, 3, 3]) = 3;
val test7 = number_before_reaching_sum(30, [1, 2, 3, 4, 5, 6, 7, 8, 9]) = 7;
val test8 = number_before_reaching_sum(50, [10, 20, 30, 40, 50, 60, 70]) = 2;



(* test for what_month *)
val test0 = what_month 70 = 3
val test1 = what_month(1) = 1;
val test2 = what_month(59) = 2;
val test3 = what_month(90) = 3;
val test4 = what_month(120) = 4;
val test5 = what_month(181) = 6;
val test6 = what_month(213) = 8;
val test7 = what_month(245) = 9;
val test8 = what_month(273) = 9;
val test9 = what_month(306) = 11;
val test10 = what_month(337) = 12;

(* test for month_range *)
val test10 = month_range (31, 34) = [1,2]
val test1 = month_range(29, 32)
(* test for oldest *)
val test0 = oldest([(2012,2,28),(2011,3,31),(2011,4,28)]) = SOME (2011,3,31)
val test1 = oldest([]) = NONE;
val test2 = oldest([(2010, 1, 1)]) = SOME (2010, 1, 1);
val test3 = oldest([(2015, 3, 14), (2015, 3, 14)]) = SOME (2015, 3, 14);
val test4 = oldest([(2010, 1, 1), (2015, 3, 14), (2011, 2, 28)]) = SOME (2010, 1, 1);
val test5 = oldest([(2010, 1, 1), (2011, 2, 28), (2012, 2, 28)]) = SOME (2010, 1, 1);
val test6 = oldest([(2011, 2, 28), (2012, 2, 28), (2011, 4, 28)]) = SOME (2011, 2, 28);

(* challenge question test *)
(* Test cases for is_contains function with integers *)
val test_case0 = is_contains([1, 2, 3], 3) = true;
val test_case1 = is_contains([4, 5, 6], 5) = true;
val test_case2 = is_contains([7, 8, 9], 10) = false;
val test_case3 = is_contains([11, 12, 13, 14, 15], 16) = false;
val test_case4 = is_contains([16, 17, 18], 19) = false;
val test_case5 = is_contains([16, 17, 18], 17) = true;

(* Test cases for remove_duplication function with integers *)
val test_case0 = remove_duplication([1, 2, 3, 3]) = [1, 2, 3];
val test_case1 = remove_duplication([1, 2, 3]) = [1, 2, 3];
val test_case2 = remove_duplication([1, 1, 1, 1]) = [1];
val test_case3 = remove_duplication([2, 3, 4, 2]) = [3, 4, 2];
val test_case4 = remove_duplication([]) = [];
val test_case5 = remove_duplication([1, 2, 3, 3, 2, 1]) = [3, 2, 1];


(* test for number_in_months_challange *)

val test0 = number_in_months_challenge([(2012,2,28),(2013,12,1),(2011,3,31),(2011,4,28)],[2,3,4,3,2]) = 3
val test1 = number_in_months_challenge([], [2, 3, 4, 3, 2]) = 0
val test2 = number_in_months_challenge([(2012, 2, 28), (2013, 12, 1)], [2, 3, 4, 3, 2]) = 1
val test3 = number_in_months_challenge([(2012, 2, 28), (2013, 12, 1), (2011, 3, 31), (2011, 4, 28)], [2, 3, 4, 3, 2]) = 3
val test4 = number_in_months_challenge([(2012, 1, 28), (2013, 12, 1)], [2, 3, 4, 3, 2]) = 0
val test5 = number_in_months_challenge([(2013, 2, 28), (2013, 2, 1), (2013, 2, 14)], [2, 3, 4, 3, 2]) = 3
val test6 = number_in_months_challenge([(2012, 2, 28), (2012, 3, 1), (2013, 4, 1)], [2, 3, 4, 3, 2]) = 3
val test7 = number_in_months_challenge([(2012, 2, 28), (2012, 3, 1), (2013, 4, 1)], [1, 5, 6, 5, 1]) = 0


(* test for dates_in_months_challenge *)
val test0 = dates_in_months_challenge ([(2012,2,28),(2013,12,1),(2011,3,31),(2011,4,28)],[2,3,4,4,3]) = [(2012,2,28),(2011,4,28),(2011,3,31)]; 
val test1 = dates_in_months_challenge ([(2000,1,1),(2001,2,2),(2002,3,3)], [1,2,3,3,2,1]) = [(2002,3,3),(2001,2,2),(2000,1,1)] ;
val test2 = dates_in_months_challenge ([(1999,11,30),(2000,12,31),(2001,1,1)], [1,2,3,3,2])  = [(2001,1,1)]
val test3 = dates_in_months_challenge ([], [1,2,3,3,2,1]) = [];
val test4 = dates_in_months_challenge ([(2022,6,15),(2022,7,20)], []) = [];
val test5 = dates_in_months_challenge ([(1998,8,31),(2000,2,29),(2022,11,30)], [6,6,7,8,7,8]) = [(1998,8,31)]

(* test for is_leap_year  *)
val test1 = is_leap_year(2016) = true
val test2 = is_leap_year(2019) = false 
val test3 = is_leap_year(2100) = false
val test4 = is_leap_year(2400) = true

(* test for reasonable_date *)

val test5 = reasonable_date(1500,2,30) =false ;
val test6 = reasonable_date(2016,2,29) = true;
val test7 = reasonable_date(2016,13,30) = false 
val test8 = reasonable_date(2016,6,31) = false
val test9 = reasonable_date(~2016,1,12) = false