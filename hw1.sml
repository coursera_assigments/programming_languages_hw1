(*  date : year * month * day *)
fun is_older(date1 : (int * int * int), date2 : (int * int * int)) = 
    if #1 date1 < #1 date2 orelse 
        #1 date1 = #1 date2 andalso #2  date1 < #2 date2 orelse
        #2 date1 = #2 date2 andalso #3 date1 < #3 date2
    then true
    else false

fun number_in_month (dates : (int * int * int) list ,month: int) = 
    if null dates
    then 0
    else 
        let 
            val count = number_in_month(tl dates, month)
        in
            if #2 (hd dates) = month
            then  count + 1
            else count
        end

fun number_in_months (dates : (int * int * int) list, months : int list) = 
    if null months 
    then 0
    else number_in_months(dates, tl months) + number_in_month(dates, hd months)


fun dates_in_month (dates: (int * int * int) list, month : int) = 
    if null dates
    then []
    else 
        let 
            val result = dates_in_month(tl dates, month)
        in
            if #2 (hd dates) = month
            then (hd dates) :: result
            else result
        end

fun dates_in_months (dates : (int * int * int) list, months : int list) = 
    if null months 
    then []
    else dates_in_month(dates, hd months) @ dates_in_months(dates, tl months) 

fun get_nth (strings: (string list) , index : int) = 
    if null strings
    then ""
    else
        if index = 1
        then hd strings
        else get_nth(tl strings, index - 1)

fun date_to_string (date : (int * int * int)) = 
    let 
        val months = [
                        "January", "February", "March", "April",
                        "May", "June", "July", "August", "September", 
                        "October", "November", "December"
                        ]
        val month_as_string = get_nth(months, #2 date)
        val new_format = month_as_string ^ " " ^ 
                            Int.toString (#3 date) ^ ", " ^ 
                            Int.toString (#1 date)
    in 
        new_format
    end

fun number_before_reaching_sum (sum : int , numbers : int list) = 
    if (sum - hd numbers) <= 0
    then 0
    else 
        let
            val count = number_before_reaching_sum(sum - hd numbers, tl numbers)
        in
            count + 1 
        end


fun what_month (day : int) =
    let
       val days_in_months = [
                                31, 28, 31, 30, 31, 30,
                                31, 31, 30, 31, 30, 31
                                ];
        val month = number_before_reaching_sum(day, days_in_months);
    in 
        month + 1
    end

(* helper function that creates an array between passed parameters *)
(* assume num1 < num2 *)
fun array_from_to(num1 : int , num2 : int) = 
    if  num1 = num2
    then [num2]
    else num1 :: array_from_to(num1 + 1, num2)



fun month_range (day1 : int , day2 : int ) =
    if day1 > day2 
    then []
    else what_month(day1) :: month_range(day1 + 1, day2)
        
fun oldest (dates : (int * int * int ) list) = 
    if null dates
    then NONE
    else 
        let 
            val answer = oldest(tl dates);
        in
            if isSome answer andalso is_older(valOf answer, hd dates)
            then answer
            else SOME (hd dates)
        end

(* challenge questions *)
    
fun is_contains(str_arr: int list , str : int ) = 
    if null str_arr
    then false
    else 
        let 
            val contains = is_contains(tl str_arr , str);
        in 
            if contains
            then true
            else if hd str_arr = str
            then true
            else false
        end 

fun remove_duplication(str_arr : int list) = 
    if null str_arr
    then []
    else  
        let 
            val duplication_answer = remove_duplication(tl str_arr);
            val ans = is_contains(duplication_answer, hd str_arr)
        in 
            if ans 
            then duplication_answer
            else hd str_arr :: duplication_answer
        end

          
            
fun number_in_months_challenge(dates : (int * int * int) list, months : int list) = 
    let 
        val new_months = remove_duplication(months)
        val answer  = number_in_months(dates, new_months)
    in 
        answer
    end


fun dates_in_months_challenge(dates : (int * int * int) list, months : int list) = 
    let 
        val new_months = remove_duplication(months)
        val answer  = dates_in_months(dates, new_months)
    in 
        answer
    end


fun is_leap_year(year : int) = 
    if year mod 4 = 0 andalso 
        (year mod 100 <> 0 orelse year mod 400 = 0) 
    then true
    else false

fun days_in_months(year) = 
    let 
        val is_leap = is_leap_year(year);
    in  
        if is_leap
        then [ 31, 29, 31, 30, 31, 30,
                31, 31, 30, 31, 30, 31
            ]
        else [ 31, 28, 31, 30, 31, 30,
                31, 31, 30, 31, 30, 31
            ]
    end 

fun get_nth_for_int_list (strings: (int list) , index : int) = 
    if null strings
    then ~1
    else
        if index = 1
        then hd strings
        else get_nth_for_int_list(tl strings, index - 1)

(* 
 *)
fun reasonable_date(date : (int * int * int )) =  
    if #1 date <= 0
    then false
    else if (#2 date) > 12 orelse (#2 date) < 1 
    then false 
    else 
        let 
            val day = #3 date; 
            val days = days_in_months(#1 date)
            val validDay = get_nth_for_int_list(days, #2 date)
        in  
            if day < 1 orelse day > validDay 
            then false
            else true
        end